# osv-scan-ci

## What is this?

Proof of concept for running [`osv-scanner`](https://github.com/google/osv-scanner) in a GitLab CI job.

## How can I test it out?

To scan a repository (and it's lockfiles), Add this to your `.gitlab-ci.yml`:

```yaml
include:
  remote: "https://gitlab.com/greg/osv-scan-ci/-/raw/main/.gitlab-ci.yml"
```

To scan an sbom file, set the `SBOM_FILE` CI/CD variable to point to the file.

To scan a debian-based container image, set the `IMAGE_TAG` CI/CD variable to the tag of the `debian:tag` image you'd like to scan.
